{
    'name': 'Janji Manis - HR Custom Settings',
    'version': '15.0.1.0.0',
    'depends': ['hr_contract'],
    'author': 'Janji Manis',
    'summary': 'Make the list of employee in the HR Responsible field in hr.contract model',
    'description': """
Change Log
==========
Version 1.0.0 (Oct 24th, 2022)
-------------------------------
Godeliva Divayu K
- Custom the domain (HR Responsible) for displaying the list of employees that responsible in contract

""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        'security/groups.xml',
        'views/hr_contract_views.xml'
    ],
    'assets': {
    },
    'auto_install': False,
    'installable': True,
    'application': False,
}
