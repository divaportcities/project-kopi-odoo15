{
    'name': 'Janji Manis - Payroll for Employee Custom',
    'version': '15.0.1.0.1',
    'depends': ['hr_payroll','hr_attendance'],
    'author': 'Janji Manis',
    'summary': 'Connecting attendances app with payslip and calculate the payslip based on the attendance hours',
    'description': """
Change Log
==========
Version 1.0.0 (October, 25th 2022)
-------------------------------
- Add a field that calculate hours of the employee attendance
Version 1.0.1 (October, 31st 2022)
-------------------------------
- Manage codes to calculate the number of days and hours 
- Adding new lines if it is overtime in the Other Input


""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        'views/hr_payslip_views.xml',
        'views/hr_contract_views.xml',
        'data/hr_payroll_data.xml',
        'data/hr_payroll_structure_data.xml',
        'data/hr_salary_rule_data.xml',
        # 'data/hr_payroll_account_data.xml',
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
}

