from odoo import api, fields, models, _

class HrPayslipWorkedDays(models.Model):
    _inherit = 'hr.payslip.worked_days'

    @api.depends('is_paid', 'number_of_hours', 'payslip_id', 'contract_id.wage', 'payslip_id.sum_worked_hours')
    def _compute_amount(self):
        result = super(HrPayslipWorkedDays, self)._compute_amount()
        for worked_days in self.filtered(lambda wd: not wd.payslip_id.edited):
            if worked_days.code == 'DEB':
                worked_days.amount = 0
                continue