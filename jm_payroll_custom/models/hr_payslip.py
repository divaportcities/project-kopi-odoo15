from odoo import api, fields, models, _


class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    sum_attendance_hours = fields.Float(string='Worked Hours', compute='_compute_attendance_hours')
    sum_absence_hours = fields.Float(string='Absence Hours', compute='_compute_absence_hours')
    sum_number_days = fields.Float(compute='_compute_number_days_hours')
    sum_number_hours = fields.Float(compute='_compute_number_days_hours')
    sum_all_hours = fields.Float(compute='_compute_all_hours', store=True)
    sum_deduction_hours = fields.Float(compute='_compute_all_hours', store=True)

    @api.depends('worked_days_line_ids.number_of_hours', 'worked_days_line_ids.is_paid', 'date_from', 'date_to')
    def _compute_attendance_hours(self):
        ''' Adds worked hours from attendances app to payslip in payroll app'''
        attendance_ids = self.env['hr.attendance'].search([
            ('employee_id', '=', self.employee_id.id),
            ('check_in', '>=', self.date_from),
            ('check_out', '<=', self.date_to),
        ])
        work_time_ids = self.env['hr.work.entry'].search(
            [('employee_id', '=', self.employee_id.id)], order='date_start asc')
        work_time = [time.date_start.date() for time in work_time_ids]

        employee_attendance = attendance_ids.filtered(lambda x: x.check_in.date() in work_time)
        if employee_attendance:
            attendance_hours = sum(
                attend.worked_hours for attend in employee_attendance if self._get_attendance_true(attend.check_in))
            self.sum_attendance_hours = attendance_hours
        else:
            self.sum_attendance_hours = 0

    @api.depends('worked_days_line_ids.number_of_hours', 'worked_days_line_ids.is_paid', 'date_from', 'date_to')
    def _compute_absence_hours(self):
        ''' Adds worked hours from attendances app to payslip in payroll app'''
        attendance_ids = self.env['hr.attendance'].search([
            ('employee_id', '=', self.employee_id.id),
            ('check_in', '>=', self.date_from),
            ('check_out', '<=', self.date_to),
        ])
        hours_per_day = self._get_worked_day_lines_hours_per_day()

        if attendance_ids:
            absence_hours = sum(
                hours_per_day for attend in attendance_ids if not self._get_attendance_true(attend.check_in))
            self.sum_absence_hours = absence_hours
        else:
            self.sum_absence_hours = 0
        
    def _get_attendance_true(self, check_in):
        employee_contract = self.env['hr.contract'].search([
            ('state', 'in', ['draft', 'open']),
            ('employee_id', '=', self.employee_id.id)
        ])
        work_time_ids = self.env['hr.work.entry'].search(
            [('employee_id', '=', self.employee_id.id)])

        if employee_contract:
            for time in work_time_ids:
                if time.date_start.date() == check_in.date():
                    time_difference = check_in - time.date_start
                    time_difference = time_difference.seconds/60
                    if time_difference > int(employee_contract.late_tolerance):
                        return False
                    return True

    @api.depends('worked_days_line_ids.number_of_hours', 'worked_days_line_ids.is_paid', 'date_from', 'date_to')
    def _compute_number_days_hours(self):
        ''' Adds worked hours from attendances app to payslip in payroll app'''
        attendance_ids = self.env['hr.attendance'].search([
            ('employee_id', '=', self.employee_id.id),
            ('check_in', '>=', self.date_from),
            ('check_out', '<=', self.date_to),
        ], order='check_in asc')
        hours_per_day = self._get_worked_day_lines_hours_per_day()

        if attendance_ids:
            for attend in attendance_ids:
                if self._get_attendance_true(attend.check_in):
                    self.sum_number_hours += hours_per_day
                    self.sum_number_days += 1
        else:
            self.sum_number_hours = self.sum_number_days = 0
    
    @api.depends('worked_days_line_ids.number_of_hours')
    def _compute_all_hours(self):
        self.ensure_one()
        self.sum_all_hours = sum(hour.number_of_hours for hour in self.worked_days_line_ids)
        self.sum_deduction_hours = sum(hour.number_of_hours for hour in self.worked_days_line_ids if hour.code =='DEB')

    def _get_overtime_input_template(self):
        '''Get the template in other input for overtime'''
        self.ensure_one()
        return self.env.ref('jm_payroll_custom.input_overtime')

    def _get_deduction_input_template(self):
        '''Get the template in other input for overtime'''
        self.ensure_one()
        return self.env.ref('jm_payroll_custom.input_deduction')
    
    def _get_deduction_calculation(self):
        deduction_input_type = self._get_deduction_input_template()
        hours_per_day = self._get_worked_day_lines_hours_per_day()
        work_entry_type = self.env['hr.work.entry.type'].browse(deduction_input_type.id)
        # deduction_amount = round(
        #     self.contract_id.contract_wage * self.sum_absence_hours / self.sum_worked_hours)
        deduction_days = self.sum_absence_hours/hours_per_day
        # self.write({
        #     'input_line_ids': [(0, 0, {
        #         'input_type_id': deduction_input_type.id,
        #         'name': 'Employee is Absence for %s hours' % self.sum_absence_hours,
        #         'amount': deduction_amount,
        #     })]
        # })
        # self.compute_sheet()

        deduction_line = {
            'sequence': work_entry_type.sequence,
            'work_entry_type_id': deduction_input_type.id,
            'number_of_days': deduction_days,
            'number_of_hours': self.sum_absence_hours,
        }
        return deduction_line

    def _get_overtime_calculation(self, hours):
        # create the overtime line in other input
        if (self.sum_attendance_hours > hours):
            overtime_input_type = self._get_overtime_input_template()
            overtime_hours = round((self.sum_attendance_hours - hours), 2)
            overtime_bonus = self.contract_id.overtime_bonus
            # calculating amount of overtime
            overtime_amount = round(overtime_bonus * overtime_hours)
            self.write({
                'input_line_ids': [(0, 0, {
                    'input_type_id': overtime_input_type.id,
                    'name': 'Employee is Overtime for %s hours' % overtime_hours,
                    'amount': overtime_amount,
                })]
            })
            self.compute_sheet()

    def _get_worked_day_lines_values(self, domain=None):
        '''Generating the value of worked days & inputs'''
        self.ensure_one()
        result = super(HrPayslip, self)._get_worked_day_lines_values(domain)
        res = []
        attendance_times = self.env['hr.attendance'].search(
            [('employee_id', '=', self.employee_id.id)])

        # calculating the attendance
        work_hours = self.contract_id._get_work_hours(
            self.date_from, self.date_to, domain=domain)
        work_hours_ordered = sorted(work_hours.items(), key=lambda x: x[1])
        for work_entry_type_id, hours in work_hours_ordered:
            work_entry_type = self.env['hr.work.entry.type'].browse(work_entry_type_id)
            attendance_line = {
                'sequence': work_entry_type.sequence,
                'work_entry_type_id': work_entry_type_id,
                'number_of_days': self.sum_number_days,
                'number_of_hours': self.sum_number_hours,
            }
            res.append(attendance_line)

            if (self.sum_number_hours >= hours):
                self.sum_number_hours = hours
                self._get_overtime_calculation(hours)
            elif (self.sum_attendance_hours < hours):
                res.append(self._get_deduction_calculation())

        return res
