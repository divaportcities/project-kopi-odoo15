from odoo import api, fields, models, _

class HrContract(models.Model):
    _inherit = "hr.contract"

    overtime_bonus = fields.Monetary('Overtime Bonus', store=True)
    late_tolerance = fields.Selection([
        ('30', '30 minutes'),
    ], string='Late Tolerance')