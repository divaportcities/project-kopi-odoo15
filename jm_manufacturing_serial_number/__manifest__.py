{
    'name': 'Janji Manis - Lot&Serial Number Manufacturing',
    'version': '15.0.1.0.1',
    'depends': ['mrp', 'jm_data_custom'],
    'author': 'Janji Manis',
    'summary': '',
    'description': """
Change Log
==========
Version 1.0.0 (Sep, 5th 2022)
-------------------------------
Godeliva Divayu K
- Custom Lot&Serial Number for manufacturing orders to automatically choose the right serial number based on the grinding option


""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        'data/mrp_batch_data.xml',
        'views/mrp_production_views.xml',
    ],
    'assets': {
    },
    'auto_install': False,
    'installable': True,
    'application': False,
}
