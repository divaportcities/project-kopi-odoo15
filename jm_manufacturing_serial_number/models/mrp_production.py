from odoo import api, fields, models, _

class MrpProduction(models.Model):
    _inherit = "mrp.production"

    grinding_option = fields.Selection([
        ('wbarabica', 'Whole Bean Coffee Arabica'),
        ('wbrobusta', 'Whole Bean Coffee Robusta'),
        ('cbarabica', 'Coarse Coffee Arabica'),
        ('cbrobusta', 'Coarse Coffee Robusta'),
        ('fbarabica', 'Fine Coffee Arabica'),
        ('fbrobusta', 'Fine Coffee Robusta'),
    ])

    def action_generate_serial(self):
        """Generating serial number automatically based on what choosen from grinding option"""
        self.ensure_one()
        name = self.env['stock.production.lot']._get_next_serial(self.company_id, self.product_id) 
        if self.grinding_option:
            # set the sequence based on the selected options
            if not name:
                if self.grinding_option == 'wbrobusta':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.whole.robusta.number')
                elif self.grinding_option == 'wbarabica':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.whole.arabica.number')
                elif self.grinding_option == 'cbrobusta':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.coarse.robusta.number')
                elif self.grinding_option == 'cbarabica':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.coarse.arabica.number')
                elif self.grinding_option == 'fbrobusta':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.fine.robusta.number')
                elif self.grinding_option == 'fbarabica':
                    name = self.env['ir.sequence'].next_by_code('mrp.batch.fine.arabica.number')

            create_values = {
                'product_id': self.product_id.id,
                'company_id': self.company_id.id,
                'name': name,
            }
            get_serial = self.env['stock.production.lot'].create(create_values)
            self.lot_producing_id = get_serial.id

        if self.move_finished_ids.filtered(lambda m: m.product_id == self.product_id).move_line_ids:
            self.move_finished_ids.filtered(
                lambda m: m.product_id == self.product_id).move_line_ids.lot_id = self.lot_producing_id
        
        if not self.lot_producing_id:
            result = super(MrpProduction, self).action_generate_serial()
            return result
        
        return True