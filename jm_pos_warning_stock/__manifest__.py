{
    'name': 'Janji Manis - Point of Sale with Quantity Mark',
    'version': '15.0.1.0.0',
    'depends': ['point_of_sale'],
    'author': 'Janji Manis',
    'summary': 'Give a warning message on PoS when the stock is running out',
    'description': """
Change Log
==========
Version 1.0.0 (September, 5th 2022)
-------------------------------
    - Created custom module "Point of Sale with Quantity Mark" give a warning message on pos
      when the quantity reaches 0.
      By: Kartiko Nurhada'

Version 1.0.1

    - Change the comparison variable to trigger the warning with forcasted quantity.
    By: Kartiko Nurhada' W.


""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
    'assets' : {
        'point_of_sale.assets' : [
            'jm_pos_warning_stock/static/src/css/pos.css', 
            'jm_pos_warning_stock/static/src/js/ProductQuantity.js',
            'jm_pos_warning_stock/static/src/js/OrderLineWarning.js',
        ],
        'web.assets_qweb': []
    }
}

