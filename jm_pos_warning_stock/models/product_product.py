from odoo import models 


class ProductProduct(models.Model):
    _inherit = "product.product"

    def get_product_info_pos(self, price, quantity, pos_config_id):
        '''
        Function to pass the quantity of product from spesific warehouse to point of sale.
        '''
        res = super(ProductProduct,self).get_product_info_pos(price, quantity, pos_config_id)
        config = self.env['pos.config'].browse(pos_config_id)
        for warehouse in res['warehouses']:
            # Check the cafe's warehouse name on warehouses_list
            if (warehouse['name'] == config.picking_type_id.warehouse_id.name):
                warehouse_data = [{
                    'id_product' : self.id,
                    'name': warehouse['name'],
                    'available_quantity': warehouse['available_quantity'],
                    'forecasted_quantity': warehouse['forecasted_quantity'],
                    'uom': warehouse['uom'],
                    'product_type': self.detailed_type,
                }]
                res['warehouses'] = warehouse_data
        return res

