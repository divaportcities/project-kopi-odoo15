odoo.define('jm_pos_warning_stock.OrderLineWarning', function(require) {
    'use.strict'
    const Models = require('point_of_sale.models');

    var core = require('web.core');
    var field_utils = require('web.field_utils');
    var { Gui } = require('point_of_sale.Gui');

    var _super_orderline = Models.Orderline.prototype;
    Models.Orderline = Models.Orderline.extend({
        set_quantity: function(quantity,warehouse, keep_price){
            if(quantity !== 'remove' && warehouse){
                var quant = typeof(quantity) === 'number' ? quantity : (field_utils.parse.float('' + quantity) || 0);
                // Condition to check the quantity and show the warning message.
                if (warehouse.forecasted_quantity  < quant && 
                    warehouse.product_type == 'product') {
                    Gui.showPopup("ErrorPopup", {
                        'title': core._t("Reach Last Product"),
                        'body':  _.str.sprintf(core._t('Cannot add more products because stock is not enough, please reduce the order.')),
                    });
                    return false;
                } else {
                    var res = _super_orderline.set_quantity.apply(this,arguments);
                    return res;
                }
            }else{ 
                var res = _super_orderline.set_quantity.apply(this,arguments);
                return res;
            }
        }
    });
});