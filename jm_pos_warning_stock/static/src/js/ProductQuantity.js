odoo.define('jm_pos_warning_stock.ProductQuantity', function(require) {
    'use strict';

    var core = require('web.core');
    const { Gui }   = require('point_of_sale.Gui');
    const NumberBuffer = require('point_of_sale.NumberBuffer');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const Registries = require('point_of_sale.Registries');

    const ProductQuantity = (ProductScreen) =>

    class extends ProductScreen {
        // Inherit _clickProduct function with OWL Method.
        async _clickProduct(event) {
            var self = this;
            const order = this.env.pos.get_order();
            // Running the method on python to get the available quantity from PoS's warehouse.
            const warehouse_quantity_pos = await self.rpc({
                model: 'product.product',
                method: 'get_product_info_pos',
                args: [[event.detail.id],
                event.detail.get_price(order.pricelist, 1),
                1,
                this.env.pos.config_id],
            })
            // Condition to check the quantity and show the warning message.
            if (warehouse_quantity_pos.warehouses[0].product_type == 'product') {
                if (warehouse_quantity_pos.warehouses[0].forecasted_quantity  < 1) {
                    Gui.showPopup("ErrorPopup", {
                        'title': core._t("Sold Out"),
                        'body':  _.str.sprintf(core._t('This item is sold out.')),
                    });
                    return false;
                } else if (this.currentOrder.selected_orderline && this.currentOrder.selected_orderline.
                    product.id == warehouse_quantity_pos.warehouses[0].id_product){
                    if (warehouse_quantity_pos.warehouses[0].forecasted_quantity  <= 
                        this.currentOrder.selected_orderline.quantity) {
                        Gui.showPopup("ErrorPopup", {
                            'title': core._t("Reach Last Product"),
                            'body':  _.str.sprintf(core._t('Cannot add more order for this product due to stock is not enough.')),
                        });
                        return false;
                    }
                } else {
                    var tempt_orderline = null;
                    _.each(this.currentOrder.orderlines.models, function(orderline) {
                        if(orderline.product.id == warehouse_quantity_pos.warehouses[0].id_product){
                            tempt_orderline = orderline
                        }
                    })
                    if (tempt_orderline && warehouse_quantity_pos.warehouses[0].forecasted_quantity  <= 
                        tempt_orderline.quantity) {
                        Gui.showPopup("ErrorPopup", {
                            'title': core._t("Reach Last Product"),
                            'body':  _.str.sprintf(core._t('Cannot add more order for this product due to stock is not enough.')),
                        });
                        return false;
                    }
                }
            }
            await super._clickProduct(event);
        }
        _setValue(val) {
            if (this.currentOrder.get_selected_orderline()) {
                if (this.state.numpadMode === 'quantity') {
                    var orderline = this.currentOrder.get_selected_orderline()
                    var self = this;
                    // Running the method on python to get the available quantity from PoS's warehouse.
                    self.rpc({
                        model: 'product.product',
                        method: 'get_product_info_pos',
                        args: [[orderline.product.id],0,1,self.env.pos.config_id],
                    })
                    .then(function (warehouse) {
                        // Pass the warehouse value to set_quantity() function.
                        const result = self.currentOrder.get_selected_orderline().set_quantity(val,warehouse.warehouses[0]);
                        if (!result) NumberBuffer.reset();
                    })
                } else if (this.state.numpadMode === 'discount') {
                    this.currentOrder.get_selected_orderline().set_discount(val);
                } else if (this.state.numpadMode === 'price') {
                    var selected_orderline = this.currentOrder.get_selected_orderline();
                    selected_orderline.price_manually_set = true;
                    selected_orderline.set_unit_price(val);
                }
            }
        }
    }
    

    Registries.Component.extend(ProductScreen, ProductQuantity);
    return ProductQuantity;
})