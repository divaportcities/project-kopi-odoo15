from odoo import api, fields, models, _

class StockLandedCost(models.Model):
    _inherit = "stock.landed.cost"

    @api.model
    def default_get(self, fields):
        """Get the value of picking and display it on the transfer field """
        res = super(StockLandedCost, self).default_get(fields)
        if self._context.get('active_model') == 'purchase.order':
            purchase_id = self.env.context.get('active_id')
            purchase_order = self.env['purchase.order'].browse(purchase_id)
            picking_ids = purchase_order.picking_ids
            if picking_ids:
                res.update({
                    'picking_ids': [(6, 0, picking_ids.ids)],
                })
        return res
