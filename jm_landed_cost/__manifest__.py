{
    'name': 'Janji Manis - Automate Transfers for Landed Cost',
    'version': '15.0.1.0.1',
    'depends': ['stock_landed_costs'],
    'author': 'Janji Manis',
    'summary': 'Automation of showing he field transfer based on the creation in the first place',
    'description': """
Change Log
==========
Version 1.0.0 (Sep, 21st 2022)
-------------------------------
Godeliva Divayu K
- Custom display the list of pickings based on the created landed cost

""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
    ],
    'assets': {
    },
    'auto_install': False,
    'installable': True,
    'application': False,
}
