from odoo import api, fields, models, _

class PosBomProduct(models.Model):
    _name = 'pos.bom.product'
    _description = 'BOM POS Product'

    name = fields.Char(store=True, readonly=False)
    product_id = fields.Many2one(
        'product.product', 'Product',
        domain="""[
            ('is_pos_bom_product','=',True),
        ]
        """, store=True,
        readonly=True, required=True)
    product_uom_qty = fields.Float(
        'Quantity', default=1,
        readonly=True, required=True)
    product_tmpl_id = fields.Many2one('product.template', 'Product Template', related='product_id.product_tmpl_id')
    product_uom_id = fields.Many2one(
        'uom.uom', 'Product UOM',
        readonly=True, required=True, related='product_id.uom_id',
        domain="[('category_id', '=', product_uom_category_id)]")
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    pos_bom_product_ids = fields.One2many(
        'pos.bom.product.line', 'pos_bom_id', 'POS BOM Lines', store=True)
    
class PosBomProductLine(models.Model):
    _name = 'pos.bom.product.line'
    _description = 'BOM POS Product Line'
    
    product_id = fields.Many2one(
        'product.product', 'Product',
        domain="""[
            ('type', 'in', ['product', 'consu']),
            ('is_pos_bom_product','!=',True),
        ]
        """, store=True,
        readonly=False, required=True)
    product_uom_qty = fields.Float(
        'Quantity', digits='Product Unit of Measure',
        readonly=False, required=True, tracking=True)
    product_uom_id = fields.Many2one(
        'uom.uom', 'Product UOM',
        readonly=False, required=True, related='product_id.uom_id', domain="[('category_id', '=', product_uom_category_id)]")
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    pos_bom_id = fields.Many2one(
        'pos.bom.product', 'Parent BoM',
        index=True, ondelete='cascade',store=True, required=True)
