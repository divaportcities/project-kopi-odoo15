from odoo import api, fields, models, _
from itertools import groupby


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def _prepare_stock_move_bom_vals(self, first_line, order_lines):
        pos_bom_result = []
        if first_line.product_id.is_pos_bom_product:
            for order in first_line:
                pos_bom = self.env['pos.bom.product'].search([('product_id', '=', order.product_id.id)])
                for bom_line in pos_bom.pos_bom_product_ids:
                    pos_bom_result.append({
                        'name': bom_line.product_id.name,
                        'product_uom': bom_line.product_uom_id.id,
                        'picking_id': self.id,
                        'picking_type_id': self.picking_type_id.id,
                        'product_id': bom_line.product_id.id,
                        'product_uom_qty': abs(sum(order_lines.mapped('qty')))*bom_line.product_uom_qty,
                        'state': 'draft',
                        'location_id': self.location_id.id,
                        'location_dest_id': self.location_dest_id.id,
                        'company_id': self.company_id.id,
                    })
        return pos_bom_result

    def _create_move_from_pos_order_lines(self, lines):
        self.ensure_one()
        lines_by_product = groupby(sorted(lines, key=lambda l: l.product_id.id), key=lambda l: l.product_id.id)
        bom_products = lines.mapped('product_id').filtered(lambda p:p.is_pos_bom_product != False)
        if bom_products:
            for product, lines in lines_by_product:
                order_lines = self.env['pos.order.line'].concat(*lines)
                current_move = self.env['stock.move'].create(
                    self._prepare_stock_move_vals(order_lines[0], order_lines)
                )
                bom_moves_values = self._prepare_stock_move_bom_vals(order_lines[0], order_lines)
                for value in bom_moves_values:
                    move = self.env['stock.move'].create(value)
                    current_move = self.env['stock.move'].concat(*move, *current_move)
                confirmed_moves = current_move._action_confirm()
                confirmed_moves._add_mls_related_to_order(order_lines)
        else:
            return super(StockPicking, self)._create_move_from_pos_order_lines(lines)
