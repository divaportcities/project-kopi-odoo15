from odoo import models


class ProductProduct(models.Model):
    _inherit = "product.product"
    
    def get_addon_product(self, product_tmpl_id):
        """Get the list add-on items"""
        self.ensure_one()
        product_template = self.env['product.template'].browse(product_tmpl_id)
        addon_items = product_template.addons

        addon_list = [
            {
                'name': addon.name,
                'category': addon.categ_id.name,
                'product_id': addon.id,
                'type': addon.type_addons,
            }
            for addon in addon_items]

        return {
            'addons': addon_list
        }
