from odoo import fields, models

class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_pos_bom_product = fields.Boolean(string="POS BOM Product")
