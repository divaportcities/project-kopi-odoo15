{
    'name': 'Janji Manis - Custom Pos Coffee Project',
    'version': '15.0.1.0.1',
    'depends': ['point_of_sale', 'sale', 'jm_data_custom'],
    'author': 'Janji Manis',
    'summary': 'Custom for coffee company to add the add-on items into their coffee as a customization product',
    'description': """
Change Log
==========
Version 1.0.0 (Sep, 5th 2022)
-------------------------------
Version 1.0.1 
- Custom add-on items when select product

Version 1.0.2
- Custom PoS Orderline for addons product is not displayed.
- Add new variable in orderline on models.js.
- Custom PoS Receipt to display the addont product different with main product.
By: Kartiko Nurhada' W.

Version 1.0.3
- Add currency format for addons product in orderscreen and receipt PoS.
By: Kartiko Nurhada' W.

""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        'security/ir.model.access.csv',
        'views/pos_bom_product_view.xml',
        'views/product_product_view.xml',
    ],
    'assets': {
        'point_of_sale.assets': [
            'jm_coffee_pos/static/src/css/custom.scss',
            'jm_coffee_pos/static/src/js/OrderlineAddonButton.js',
            'jm_coffee_pos/static/src/js/AddonItemPopup.js',
            'jm_coffee_pos/static/src/js/addons.js',
        ],
        'web.assets_qweb': [
            'jm_coffee_pos/static/src/xml/OrderlineAddonButton.xml',
            'jm_coffee_pos/static/src/xml/AddonItemPopup.xml',
            'jm_coffee_pos/static/src/xml/Orderline.xml',
            'jm_coffee_pos/static/src/xml/OrderReceiptAddons.xml',
        ],
    },
    'auto_install': False,
    'installable': True,
    'application': False,
}
