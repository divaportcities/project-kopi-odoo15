odoo.define('jm_coffee_pos.addons', function (require) {
"use strict";

    var models = require('point_of_sale.models');

    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function(attr, options) {
            _super_orderline.initialize.call(this,attr,options);
            this.addons = this.addons || [];
            this.is_addon = this.is_addon || false ;
        },
        set_addon: function (addons) {
            this.addons = addons;
            this.trigger('change',this);
        },
        set_is_addon: function (is_addon) {
            this.is_addon = is_addon;
        },
        get_addon: function(addons){
            return this.addons;
        },
        get_is_addon: function(){
            return this.is_addon;
        },
        can_be_merged_with: function(orderline) {
            if (orderline.product.type == 'consu'){
                if (orderline.get_addon() !== this.get_addon()) {
                    return false;
                } else if (orderline.get_is_addon() !== this.get_is_addon()){
                    return false;
                } else {
                    return _super_orderline.can_be_merged_with.apply(this,arguments);
                }
            } else {
                return _super_orderline.can_be_merged_with.apply(this,arguments);
            }
        },
        clone: function(){
            var orderline = _super_orderline.clone.call(this);
            orderline.addons = this.addons;
            orderline.is_addon = this.is_addon;
            return orderline;
        },
        export_as_JSON: function(){
            var json = _super_orderline.export_as_JSON.call(this);
            json.addons = this.addons;
            json.is_addon = this.is_addon;
            return json;
        },
        init_from_JSON: function(json){
            _super_orderline.init_from_JSON.apply(this,arguments);
            this.addons = json.addons;
            this.is_addon = json.is_addon;
        },
        export_for_printing: function() {
            var line = _super_orderline.export_for_printing.apply(this,arguments);
            line.addons = this.get_addon();
            line.is_addon = this.get_is_addon();
            return line;
        },
    });
});
