odoo.define('jm_coffee_pos.OrderlineAddonButton', function (require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');
    const models = require('point_of_sale.models');
    const { useState, useRef } = owl.hooks;

    class OrderlineAddonButton extends PosComponent {
        constructor() {
            super(...arguments);
            useListener('click', this.onClick);
            this.addons = useState({})
        }
        // async willStart() {
        //     let res = this.env.pos.get_order().orderlines.models.filter(function(el){
        //         return el.is_addon == true
        //     })
        //     res.hide()
        // }
        get selectedOrderline() {
            return this.env.pos.get_order().get_selected_orderline();
        }
        addonSave(product_id) {
            var orderline_check = this.env.pos.get_order().orderlines.models
            let addons = _.filter(orderline_check, function (addon) {
                return addon.product.id == product_id
            })
            return addons
        }
        removeUnselectedAddon(inputAddon) {
            var self = this
            let res = this.env.pos.get_order().orderlines.models.filter(function(el){
                return el.is_addon == true
            })
            let orderline_addons = res.map(p => p.product.id)
            let current_addons = inputAddon.map(p => parseInt(p.product_id))
            let value = orderline_addons.filter(x => !current_addons.includes(x));
            _.each(res, function(el){
                // el.hide()
                for (let i = 0; i < value.length; i++) {
                    let test = el.product.id == value[i]
                    if (test) {
                        const order = self.env.pos.get_order()
                        self.env.pos.get_order().orderlines.models.filter(function(val){
                            if (val == el) {
                                order.remove_orderline(el)
                            }
                        })
                    }
                }
            })
        }
        async onClick() {
            if (!this.selectedOrderline) return;

            const { confirmed, payload: inputAddon } = await this.showPopup('AddonItemPopup', {
                startingValue: this.selectedOrderline.get_addon(),
                title: this.env._t('Add-on Items'),
            });

            if (confirmed) {
                // const order = this.env.pos.get_order();
                // order.remove_orderline(order.get_orderlines());
                this.selectedOrderline.set_addon(inputAddon);
                var self = this
                _.each(inputAddon, function (data) {
                    let selected_addon = data['product_id']
                    let product_addon = self.env.pos.db.get_product_by_id(selected_addon)
                    let order = self.env.pos.get_order();
                    let new_line = new models.Orderline({}, {
                        pos: self.env.pos,
                        order: order,
                        product: product_addon,
                        description: product_addon.display_name,
                        price: product_addon.lst_price,
                        tax_ids: product_addon.taxes_id,
                        price_manually_set: true,
                    });

                    new_line.is_addon = true
                    let exists = self.addonSave(data.product_id).length
                    if (exists == 0) {
                        order.add_orderline(new_line)
                    }
                })
                self.removeUnselectedAddon(inputAddon)
            }
        }
    }
    OrderlineAddonButton.template = 'OrderlineAddonButton';

    ProductScreen.addControlButton({
        component: OrderlineAddonButton,
        condition: function() {
            return true;
        },
    });

    Registries.Component.add(OrderlineAddonButton);

    return OrderlineAddonButton;
});
