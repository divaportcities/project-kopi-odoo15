odoo.define('jm_coffee_pos.AddonItemPopup', function(require) {
    'use strict';

    const AbstractAwaitablePopup = require('point_of_sale.AbstractAwaitablePopup');
    const Registries = require('point_of_sale.Registries');
    const { _lt } = require('@web/core/l10n/translation');
    var { Gui } = require('point_of_sale.Gui');
    var core    = require('web.core');
    var _t      = core._t;

    // formerly TextAreaPopupWidget
    // IMPROVEMENT: This code is very similar to TextInputPopup.
    //      Combining them would reduce the code.
    class AddonItemPopup extends AbstractAwaitablePopup {
        /**
         * @param {Object} props
         * @param {string} props.startingValue
         */
        constructor() {
            super(...arguments);
            this.SelectedOrderline = this.env.pos.get_order().get_selected_orderline();
            this.ProductAddon = false;
            this.AddonValue = [];
            this.Addonids = [];
            this.AddonSelected = [];
        }
        async willStart() {
            try {
                let selected_product = this.SelectedOrderline;
                var self = this;
                this.AddonInfo = await this.rpc({
                    model: 'product.product',
                    method: 'get_addon_product',
                    args: [[selected_product.product.id], selected_product.product.product_tmpl_id]
                }).then(function (data) {
                    if (data.addons.length > 0) {
                        _.each(data.addons, function (addon) {
                            var selected = self.get_addon_by_product_id(addon.product_id);
                            addon['selected'] = (selected.length > 0) ? true : false;
                        });
                    }
                    else {
                        Gui.showPopup('ErrorPopup', {
                            'title': _t("No Add-on"),
                            'body': _t("Product has no option for add-on items."),
                        });
                    }
                    return data;
                });
                this.AddonSelected = this.props.startingValue
                let addon_ids = this.Addonids
                if (this.AddonSelected) {
                    _.each(this.AddonSelected, function (val) {
                        addon_ids.push(val['product_id'])
                    })
                }
            } catch (error) {
                this.error = error
            }
        }
        get_addon_by_product_id(product_id) {
            var addons = this.props.startingValue;
            addons = _.filter(addons, function (addon) {
                return addon['product_id'] == product_id
            })
            return addons
        }
        _onClickAddonItem(event) {
            var target = event.currentTarget
            var addon_ids = this.Addonids
            let addon_id = target.dataset.addonId

            if ($(target).parent().parent('.addon-row').hasClass("selected")) {
                if (addon_ids) {
                    var index = _.findIndex(addon_ids, function (val) { return val === addon_id; });
                    addon_ids.splice(index,1)
                }
                $(target).parent().parent('.addon-row').removeClass("selected")
            } else {
                $(target).parent().parent('.addon-row').addClass("selected")
                addon_ids.push(addon_id)
            }
        }
        getPayload() {
            let data = this.Addonids
            var self = this
            var addon_value = this.AddonValue
            if (data) {
                _.each(data, function (val) {
                    let product_addon = self.env.pos.db.get_product_by_id(val)
                    var addons = {
                        'name': product_addon.display_name,
                        'product_id': val,
                        'product': product_addon,
                        'price': product_addon.lst_price,
                        'selected': true,
                        'is_addons' : true,
                    }
                    addon_value.push(addons)
                })
            }
            return this.AddonValue
        }
    }
    AddonItemPopup.template = 'AddonItemPopup';
    AddonItemPopup.defaultProps = {
        confirmText: _lt('Ok'),
        cancelText: _lt('Cancel'),
        title: '',
        body: '',
    };

    Registries.Component.add(AddonItemPopup);

    return AddonItemPopup;
});
