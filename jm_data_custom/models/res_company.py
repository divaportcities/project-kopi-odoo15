from odoo import api, models, _ 


class ResCompany(models.Model):
    _inherit = "res.company"

    @api.model
    def get_company_ids(self):
        '''
        Function to assign the company_ids on user.
        '''
        user_list = self.env['res.users'].search([])
        company_list = self.env['res.company'].search([])
        for user in user_list:
            user.update({
                'company_ids': [(6,0,company_list.ids)]
            })
        return {}

    @api.model
    def change_company_name(self):
        '''
        Function to assign the company_ids on user.
        '''
        company = self.env.ref('base.main_company')
        company.update({
            'name': 'Janji Manis',
            'currency_id' : self.env.ref('base.IDR'),
            'country_id' : self.env.ref('base.id'),
            'account_journal_payment_debit_account_id' : self.env.ref('jm_data_custom.jm_account_outstanding_debit').id,
            'account_journal_payment_credit_account_id' : self.env.ref('jm_data_custom.jm_account_outstanding_credit').id,
            'account_journal_suspense_account_id' : self.env.ref('jm_data_custom.jm_account_bank_suspense').id,
        })
        company_manufacture = self.env.ref('jm_data_custom.janji_manis_manufacture_company')
        company_manufacture.update({
            'account_journal_payment_debit_account_id' : self.env.ref('jm_data_custom.jmm_account_outstanding_debit').id,
            'account_journal_payment_credit_account_id' : self.env.ref('jm_data_custom.jmm_account_outstanding_credit').id,
            'account_journal_suspense_account_id' : self.env.ref('jm_data_custom.jmm_account_bank_suspense').id,
        })
        company_cafe = self.env.ref('jm_data_custom.janji_manis_cafe_company')
        company_cafe.update({
            'account_journal_payment_debit_account_id' : self.env.ref('jm_data_custom.jmc_account_outstanding_debit').id,
            'account_journal_payment_credit_account_id' : self.env.ref('jm_data_custom.jmc_account_outstanding_credit').id,
            'account_journal_suspense_account_id' : self.env.ref('jm_data_custom.jmc_account_bank_suspense').id,
        })
        return {}