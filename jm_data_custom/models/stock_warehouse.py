from odoo import api, models, _ 


class StockWarehouse(models.Model):
    _inherit = "stock.warehouse"

    @api.model
    def change_warehouse_name(self):
        '''
        Function to assign the company_ids on user.
        '''
        warehouse = self.env.ref('stock.warehouse0')
        warehouse.update({
            'name': 'Janji Manis',
            'code' : 'Janji',
        })
        return {}