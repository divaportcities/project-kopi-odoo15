from odoo import api, fields, models, _ 


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    @api.model
    def default_get(self, fields):
        '''
        To update the inventory management to 'real'
        '''
        res = super(ResConfigSettings, self).default_get(fields)

        res.update({
            'update_stock_quantities': 'real'
        })

        return res