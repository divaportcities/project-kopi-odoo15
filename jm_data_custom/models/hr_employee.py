import requests
import base64
from odoo import api, fields, models, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    product_image_url = fields.Char(string="Product Image URL")

    @api.model
    def create(self, values):
        ''' Set product image from URL when creating a new employee'''
        res = super(HrEmployee, self).create(values)
        url = values.get('product_image_url', False)
        image = False
        if url:
            image = base64.b64encode(requests.get(url).content)
        res.image_1920 = image
        return res

    @api.onchange('product_image_url')
    def _set_product_image_from_url(self):
        ''' Set employee's image from URL when changing the URL '''
        image = False
        if self.product_image_url:
            image = base64.b64encode(requests.get(
                self.product_image_url).content)
        self.image_1920 = image

class HrEmployeePublic(models.Model):
    _inherit = 'hr.employee.public'

    product_image_url = fields.Char(string="Product Image URL")