import requests
import base64
from odoo import models, fields, api, _


class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_addons = fields.Boolean(string='Is Addons')
    has_addons = fields.Boolean(string='Has Addons')
    addons = fields.Many2many("product.product")
    type_addons = fields.Selection([
        ('syrup', 'Syrups'),
        ('milk', 'Milk'),
        ('espresso', 'Espresso'),
        ('other', 'Others'),
    ])
    product_image_url = fields.Char(string='Product Image URL')

    @api.onchange('is_addons')
    def _change_type_is_addons(self):
        ''' Changes product to be available in PoS session 'Addons' category '''
        for record in self:
            if record.is_addons == True:
                self.update({
                    'sale_ok': True,
                    'available_in_pos': True,
                    'pos_categ_id': self.env.ref('jm_data_custom.pos_category_addons').id
                    })
            elif record.is_addons == False:
                self.update({
                    'available_in_pos': False,
                    })

    @api.model
    def create(self, values):
        ''' Set product image from URL when creating a new product'''
        res = super(ProductTemplate, self).create(values)
        url = values.get('product_image_url', False)
        image = False
        if url:
            image = base64.b64encode(requests.get(url).content)
        res.image_1920 = image
        return res

    @api.onchange('product_image_url')
    def _set_product_image_from_url(self):
        ''' Set product image from URL when changing the URL '''
        image = False
        if self.product_image_url:
            image = base64.b64encode(requests.get(self.product_image_url).content)
        self.image_1920 = image
