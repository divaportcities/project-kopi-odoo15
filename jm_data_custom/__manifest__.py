{
    'name': 'Janji Manis - Data Custom',
    'version': '15.0.1.0.3',
    'depends': ['stock_account', 'purchase_stock','hr', 'point_of_sale', 'product_expiry', 'account_accountant', 'sale_management'],
    'author': 'Janji Manis',
    'summary': 'Customization for importing the employees and products data',
    'description': """
Change Log
==========
Version 1.0.0 (August, 5th 2022)
-------------------------------
- Add a file csv for importing product data, including the image

Version 1.0.1 (October, 27th 2022)
-------------------------------
- Add a file csv for importing employee data, including the image

Version 1.0.2 (November, 17th 2022)
-------------------------------
- Add new config settings to activate lot serial and expiration date
- Update products_data.csv
- Update product_template_data.xml
- Add department_data.csv
- Add new dependency to prevent error when installing module

Version 1.0.3 (November, 28th 2022)
-------------------------------
- Add new dependency
""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        'data/company_data.xml',
        'data/account_data.xml',
        'data/account_journal_data.xml',
        'data/res_config_settings_data.xml',
        'data/pos_payment_method_data.xml',
        'data/product_template_data.xml',
        'data/invoke_method.xml',
        'views/product_template_views.xml',
        'views/hr_employee_views.xml',
        'views/res_company_views.xml',
        'security/groups.xml',
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
}

