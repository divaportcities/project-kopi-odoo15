{
    'name': 'Janji Manis - Website Module Custom',
    'version': '15.0.1.0.0',
    'depends': ['website', 'website_sale'],
    'author': 'Janji Manis',
    'summary': 'Module customization website for Janji Manis Cafe',
    'description': """
Change Log
==========
Version 1.0.0 (October, 20th 2022)
-------------------------------
- Custom module for website template.

    By: Kartiko Nurhada'


""",
    'website': 'http://www.portcities.net',
    'category': '',
    'sequence': 1,
    'data': [
        "views/website_sale_stock_views.xml",
        "views/website_template_views.xml",
        ],
    'auto_install': False,
    'installable': True,
    'application': False,
    'assets' : {
        'web.assets_frontend': [
            'jm_website_custom/static/src/css/website.css'
        ]
    }
}

