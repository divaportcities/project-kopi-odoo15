from odoo.addons.portal.controllers.portal import CustomerPortal
from odoo import http
from odoo.http import request, route


class CustomerPortal(CustomerPortal):

    # Routing home page
    @route(['/'], type='http', auth="public", website=True)
    def home_page(self):
        values = {}

        return request.render("jm_website_custom.website_home_template", values)

    # Routing contact us page
    @route(['/contactus'], type='http', auth="public", website=True)
    def contact_us_page(self):
        values = {}

        return request.render("jm_website_custom.website_contact_us_template", values)

    # Routing about us page
    @route(['/aboutus'], type='http', auth="public", website=True)
    def habout_us_page(self):
        values = {}

        return request.render("jm_website_custom.website_about_us_template", values)
