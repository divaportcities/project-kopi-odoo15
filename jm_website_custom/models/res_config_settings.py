from odoo import api, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    @api.model
    def default_get(self, fields):
        '''
        To update the pricelist setting to 'advanced'
        '''
        res = super(ResConfigSettings, self).default_get(fields)
        warehouse = self.env.ref('jm_data_custom.kota_lama_warehouse')
        if warehouse:
            res.update({
                'website_warehouse_id': warehouse
            })

        return res